<?php
/**
 * @category    Magebit
 * @package     Magebit_Whitelister
 * @author      Mairis Kimenis, Arturs Kruze <info@magebit.com>
 */
class Magebit_Whitelister_Block_Adminhtml_Blocks extends Mage_Adminhtml_Block_Widget_Grid_Container
{
	public function __construct()
	{
		$this->_blockGroup = 'magebit_whitelister';
		$this->_controller = 'adminhtml_blocks';
		$this->_headerText = Mage::helper('magebit_whitelister')->__('Manage blocks');

		parent::__construct();

		$this->updateButton('add', 'label', $this->__('Whitelist all'));
		$this->updateButton('add', 'onclick', "setLocation('{$this->getUrl('*/*/whitelistAll')}')");

		$this->_addButton('rescan', array(
			'label'   => $this->__('Rescan blocks'),
			'onclick' => "setLocation('{$this->getUrl('*/*/rescan')}')",
		) );
	}

}