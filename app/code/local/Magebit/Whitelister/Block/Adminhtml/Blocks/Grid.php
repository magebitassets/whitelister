<?php
/**
 * @category    Magebit
 * @package     Magebit_Whitelister
 * @author      Mairis Kimenis, Arturs Kruze <info@magebit.com>
 */
class Magebit_Whitelister_Block_Adminhtml_Blocks_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
	public function __construct()
	{
		parent::__construct();
		$this->setId('collection_block');
		$this->setDefaultSort('block');
		$this->setDefaultDir(Varien_Data_Collection::SORT_ORDER_ASC);
		$this->setUseAjax(true);
		$this->setDefaultLimit(30);
	}


	/**
     * Gets blocks collection
     *
	 * @return Magebit_Whitelister_Model_Resource_Collection_Block|Varien_Data_Collection
	 **/
	public function getCollection()
	{
		if (is_null($this->_collection)) {
			$this->_collection = Mage::getResourceSingleton('magebit_whitelister/collection_block');
		}

		return $this->_collection;
	}

    /**
     * Prepares columns to be rendered
     *
     * @return mixed
     */
	protected function _prepareColumns()
	{
		$helper = Mage::helper('magebit_whitelister');

		$this->addColumn('block', array(
			'header' => $helper->__('Block'),
			'type'   => 'text',
			'index'  => 'block'
		) );

		$this->addColumn('is_allowed', array(
			'header'  => $helper->__('Status'),
			'index'   => 'is_allowed',
			'type'    => 'options',
			'options' => array( 0 => $helper->__('Blacklisted'), 1 => $helper->__('Whitelisted') ),
		) );

		return parent::_prepareColumns();
	}

	/**
	 * Grid AJAX URL
	 *
	 * @return string
	 */
	public function getGridUrl()
	{
		return $this->getUrl('*/*/grid', array('_current' => true));
	}

	public function getRowUrl()
	{
		return null;
	}

    /**
     * Prepare function for mass actions
     *
     * @return $this
     */
	protected function _prepareMassaction()
	{
		$this->getMassactionBlock()->setUseSelectAll(false);
		$this->setMassactionIdField('block');
		$this->getMassactionBlock()->setFormFieldName('block');

		$this->getMassactionBlock()->addItem('whitelist', array(
			'label'   => Mage::helper('magebit_whitelister')->__('Whitelist'),
			'url'     => $this->getUrl( '*/*/massWhitelist', array('' => '')),
			'confirm' => Mage::helper('magebit_whitelister')->__('Are you sure?')
		) );

		$this->getMassactionBlock()->addItem('blacklist', array(
			'label'   => Mage::helper('magebit_whitelister')->__('Blacklist'),
			'url'     => $this->getUrl('*/*/massBlacklist', array('' => '')),
			'confirm' => Mage::helper('magebit_whitelister')->__('Are you sure?')
		) );

		return $this;
	}
}