<?php
/**
 * @category    Magebit
 * @package     Magebit_Whitelister
 * @author      Mairis Kimenis, Arturs Kruze <info@magebit.com>
 */
class Magebit_Whitelister_Helper_Data extends Mage_Core_Helper_Abstract
{
	/**
	 * Holds all block type list
	 *
	 * @var Convenient_Data_Collection_NoDb
	 */
	protected $blocks = array();

	/**
	 * List of cached blocks from db
	 *
	 * @var array
	 */
	protected $cached = array();

	/**
	 * Whitelists the given block
	 *
	 * @param Varien_Object $block
	 * @return void
	 */
	public function whitelistBlock(Varien_Object $block)
	{
		$this->updateBlockStatus($block, 1);
	}

	/**
	 * Blacklists the given block
	 *
	 * @param Varien_Object $block
	 * @return void
	 */
	public function blacklistBlock(Varien_Object $block)
	{
		$this->updateBlockStatus($block, 0);
	}

	/**
	 * Parses files, pages and finds all blocks
	 *
	 * @return array
	 */
	public function getAllBlocks()
	{
		if ($this->blocks) {
			return $this->blocks;
		}

		$blockModels = Mage::getConfig()->getNode()->xpath('//global/blocks');

		$this->loadCached();

		foreach ($blockModels[ 0 ] as $block => $model) {
			$class     = $model->getClassName();

			if( !$class )
			{
				continue;
			}

			$parts     = explode('_', $class);
			$extension = sprintf('%s_%s', $parts[0], $parts[1]);
			$baseDir   = Mage::getModuleDir('', $extension) . DS . implode(DS, array_slice($parts, 2));

			if (!is_dir($baseDir) || $block == 'adminhtml') {
				continue;
			}

			$this->walkDir($baseDir, array($block));
		}

		return $this->blocks;
	}

	/**
	 * Walks current directory and if a block is found, adds it to the list
	 *
	 * @param $dir
	 * @param $block
	 * @return void
	 */
	protected function walkDir($dir, $block)
	{
		$files = scandir($dir);

		foreach ($files as $file) {
			if (in_array($file, array('.', '..'))) {
				continue;
			}

			if (is_dir($dir . DS . $file)) {
				$this->walkDir($dir . DS . $file, array_merge($block, array($file)));
			} elseif (strpos($file, '.php') !== false) {
				$file = substr($file, 0, -4);
				$add  = new Varien_Object();
				$add->setData('block', sprintf('%s/%s', $block[0], implode('_', array_map('strtolower', array_merge(array_slice($block, 1), array(strtolower($file)))))));

				if (isset($this->cached[$add->getBlock()])) {
					$add->setData('is_allowed', $this->cached[$add->getBlock()]);
					$add->setData('exists', true);
				} else {
					$add->setData('is_allowed', 0);
					$add->setData('exists', false);
				}

				$this->blocks[] = $add;
			}
		}
	}

	/**
	 * Loads blocks from database
	 *
	 * @return void
	 */
	protected function loadCached()
	{
		$resource = Mage::getSingleton('core/resource');
		$read     = $resource->getConnection('core_read');

		try {
			$blocksTable = $resource->getTableName('admin/permission_block');
			if ($read->isTableExists($blocksTable)) {
				$sql         = "SELECT * FROM " . $blocksTable;
				$permissions = $read->fetchAll($sql);
				foreach ($permissions as $permission) {
					$this->cached[$permission['block_name']] = $permission['is_allowed'];
				}
			} else {
				$blocksTable = null;
			}
		} catch (Exception $e) {
			// Exception means the whitelist doesn't exist yet, or we otherwise failed to read it in. That's okay. Move on.
			$blocksTable = null;
		}
	}

	/**
	 * Updates status of the given block
	 *
	 * @param Varien_Object $block
	 * @param $status
	 * @return void
	 */
	protected function updateBlockStatus(Varien_Object $block, $status)
	{
		$resource    = Mage::getSingleton('core/resource');
		$read        = $resource->getConnection('core_read');
		$write       = $resource->getConnection('core_write');

		try {
			$blocksTable = $resource->getTableName('admin/permission_block');

			if ($read->fetchOne('SELECT 1 FROM `' . $blocksTable . '` WHERE block_name = :block', array(
					'block' => $block->getBlock())
			)
			) {
				$write->query('UPDATE `' . $blocksTable . '` SET is_allowed = :status WHERE block_name = :block', array(
					'status' => $status,
					'block'  => $block->getBlock()
				));
			} else {
				$write->insert($blocksTable, array(
					'block_name' => $block->getBlock(),
					'is_allowed' => $status
				) );
			}
		} catch (Exception $e) {
			// Exception means the whitelist doesn't exist yet, or we otherwise failed to read it in. That's okay. Move on.
			$blocksTable = null;
		}
	}

    /**
     * Clears block cache
     *
     * @return void
     */
	public function clearBlockCache()
	{
		Mage::app()->getCacheInstance()->remove('magebit_whitelister_collection_block');
	}
}