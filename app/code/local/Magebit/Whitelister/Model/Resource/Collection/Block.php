<?php
/**
 * @category    Magebit
 * @package     Magebit_Whitelister
 * @author      Mairis Kimenis, Arturs Kruze <info@magebit.com>
 */
class Magebit_Whitelister_Model_Resource_Collection_Block extends Convenient_Data_Collection_NoDb
{
	protected $objectCachedData = null;

	/**
	 * Create a filterable admin panel grid from blocks
	 *
	 * @return $this|array
	 */
	protected function fetchData()
	{
		if (is_null( $this->objectCachedData)) {
			$data = $this->getFromCache();
			if (!$data) {
				$data = Mage::helper('magebit_whitelister')->getAllBlocks();
				$this->saveToCache($data);
			}

			$this->objectCachedData = $data;
		}

		return $this->objectCachedData;
	}

	/**
	 * Gets cached block info
	 *
	 * @return bool|mixed
	 */
	public function getFromCache()
	{
		$data = $this->getCacheInstance()->load('magebit_whitelister_collection_block');
		if ($data) {
			return unserialize($data);
		}

		return false;
	}

	/**
	 * Saves to cache
	 *
	 * @param $data
	 */
	public function saveToCache($data)
	{
		$this->getCacheInstance()->save(
			serialize($data),
			'magebit_whitelister_collection_block',
			array(),
			3600
		);
	}

	/**
	 * Gets cache instance
	 *
	 * @return Mage_Core_Model_Cache
	 */
	protected function getCacheInstance()
	{
		return Mage::app()->getCacheInstance();
	}
}