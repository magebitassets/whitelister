<?php
/**
 * @category    Magebit
 * @package     Magebit_Whitelister
 * @author      Mairis Kimenis, Arturs Kruze <info@magebit.com>
 */
class Magebit_Whitelister_Adminhtml_WhitelisterController extends Mage_Adminhtml_Controller_Action
{
	/**
	 * All block list
	 *
	 * @var array
	 */
	protected $blocks = array();

	/**
	 * Common layout DRY
	 *
	 * @return $this
	 */
	protected function _initAction()
	{
		$this->loadLayout()
			 ->_setActiveMenu('magebit_whitelister/manage')
			 ->_addBreadcrumb(
				 Mage::helper('adminhtml')->__('Magebit Whitelister'),
				 Mage::helper('adminhtml')->__('Manage blocks')
			 );

		return $this;
	}

	/**
	 * Parses files, pages and finds all blocks
	 *
	 * @return void
	 */
	protected function getAllBlocks()
	{
		$this->blocks = Mage::helper('magebit_whitelister')->getAllBlocks();
	}

	/**
	 * Lists all blocks with their respective statuses
	 *
	 * @return void
	 */
	public function indexAction()
	{
		$this->_title($this->__('Whitelister'));
		$this->_title($this->__('Manage blocks'));
		$this->_initAction();

		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
		$this->_addContent($this->getLayout()->createBlock('magebit_whitelister/adminhtml_blocks'));

		$this->renderLayout();
	}


	/**
	 * Shows blocks in the grid
	 *
	 * @return void
	 */
	public function gridAction()
	{
		$this->loadLayout();
		$this->getResponse()->setBody(
			$this->getLayout()->createBlock('magebit_whitelister/adminhtml_blocks_grid')->toHtml()
		);
	}

	/**
	 * Whitelists ALL found blocks
	 *
	 * @return void
	 * @throws Exception
	 */
	public function whitelistAllAction()
	{
		$blocks = Mage::getResourceSingleton('magebit_whitelister/collection_block');

		foreach ($blocks as $block) {
			Mage::helper('magebit_whitelister')->whitelistBlock($block);
		}

		Mage::helper('magebit_whitelister')->clearBlockCache();
		Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('magebit_whitelister')->__('All found blocks were whitelisted'));

		$this->_redirect('adminhtml/whitelister');
	}

	/**
	 * Rescans all blocks
	 *
	 * @return void
	 * @throws Exception
	 */
	public function rescanAction()
	{
		Mage::helper('magebit_whitelister')->clearBlockCache();

		// rescan blocks
		$blocks = Mage::getResourceSingleton('magebit_whitelister/collection_block')->load();
		Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('magebit_whitelister')->__("%d blocks found", $blocks->count() ) );

		$this->_redirect('adminhtml/whitelister');
	}

	/**
	 * Whitelists selected blocks
	 *
	 * @return void
	 */
	public function massWhitelistAction()
	{
		$blockNames = $this->getRequest()->getParam('block');

		if (!is_array($blockNames)) {
			Mage::getSingleton('adminhtml/session')->addError($this->__('Please select blocks'));
			$this->_redirect('adminhtml/whitelister');

			return;
		}

		$updated = 0;
		$blocks  = Mage::getResourceSingleton('magebit_whitelister/collection_block');

		foreach ($blocks as $block) {
			if (in_array($block->getBlock(), $blockNames)) {
				Mage::helper('magebit_whitelister')->whitelistBlock($block);
				$updated++;
			}
		}

		Mage::helper( 'magebit_whitelister' )->clearBlockCache();

		Mage::getSingleton('adminhtml/session')->addSuccess($this->__( "%d blocks were whitelisted!", $updated));
		$this->_redirect('adminhtml/whitelister');
	}

	/**
	 * Blacklists selected blocks
	 *
	 * @return void
	 */
	public function massBlacklistAction()
	{
		$blockNames = $this->getRequest()->getParam('block');

		if (!is_array($blockNames)) {
			Mage::getSingleton('adminhtml/session')->addError($this->__('Please select blocks'));
			$this->_redirect('adminhtml/whitelister');

			return;
		}

		$updated = 0;
		$blocks  = Mage::getResourceSingleton('magebit_whitelister/collection_block');

		foreach ($blocks as $block) {
			if (in_array($block->getBlock(), $blockNames)) {
				Mage::helper('magebit_whitelister')->blacklistBlock($block);
				$updated++;
			}
		}

		Mage::helper('magebit_whitelister')->clearBlockCache();

		Mage::getSingleton('adminhtml/session')->addSuccess($this->__("%d blocks were blacklisted!", $updated));
		$this->_redirect('adminhtml/whitelister');
	}
}