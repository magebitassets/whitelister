<?php
/**
 * @category    Magebit
 * @package     Magebit_Whitelister
 * @author      Mairis Kimenis, Arturs Kruze <info@magebit.com>
 */
$this->startSetup();

$blocks = Mage::getResourceSingleton('magebit_whitelister/collection_block');

foreach ($blocks as $block) {
	Mage::helper('magebit_whitelister')->whitelistBlock($block);
}

Mage::helper('magebit_whitelister')->clearBlockCache();

$this->endSetup();